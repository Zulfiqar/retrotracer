# RetroTracer
The RetroTracer is a copy of the Realistic Ray Tracer form the book by Peter Shirley and Keith Morley. It is my little test bed to learn c++ programming. It will include, someday, support for Partio, OpenSubdiv, OpenVDB and openImageIO, and make use of threading/opencl where possible.
