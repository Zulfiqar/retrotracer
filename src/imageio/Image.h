// image.h

#ifndef _IMAGE_H_
#define _IMAGE_H_ 1

#include <cmath>
#include <string>
#include <fstream>
#include "../utilities/rgb.h"


class Image {
    public:
	Image();
	// Initializes raster to default rgb color
	Image(int width, int height);
	// Initializes raster to 'background'
	Image(int width, int height, rgb background);
	// Retruns false if x or y are out of bounds, else true
	bool set (int x, int y, const rgb & color);
	void gammaCorrect(float gamma);
	// Creates image and returns file handler
	void createFile(std::fstream & imagefile, std::string filename, bool writeflag);
	// Outputs PPM image to 'out'
	void writePPM(std::fstream & out);
	void readPPM(std::string file_name);

    private:
	rgb** raster;
	int nx;	// raster width
	int ny; // raster height
};

#endif // _IMAGE_H_
