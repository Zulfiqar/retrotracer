// DynSphere.h

#ifndef _DYNSHPERE_H
#define _DYNSPHERE_H_ 1

#include "Shape.h"
#include "../math/Vector3.h"
#include "../math/Ray.h"
#include "../utilities/rgb.h"

class DynSphere : public Shape {
    public:
        DynSphere(const Vector3& _ocenter, float _radius, const rgb& _color,
                  float mintime, float maxtime);
        bool hit(const Ray& r, float tmin, float tmax, float time, HitRecord& record) const;
        bool shadowHit(const Ray& r, float tmin, float tmax, float time) const;
        Vector3 getCenter(float time) const;

        Vector3 ocenter;
        float mintime;
        float maxtime;
        float radius;
        rgb color;
};

#endif // _DYNSPHERE_H_
