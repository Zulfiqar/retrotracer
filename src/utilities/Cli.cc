// Cli.cc

#include "Cli.h"

class Vector3;

Cli::Cli() : filename("image01.ppm"), nx(640), ny(480), st("jitter"), samples(1), 
	     filt("box"), eye(.0f, .0f, .0f), gaze(.0f, .0f, -1.0f), distance(1.0f),
             fnumber(5.6f), focal(50.0f), filmw(36.0f), filmh(24.0f), tmin(0.000001f),
             tmax(100000.0f) {}

Cli::Cli(std::string _filename, int _nx, int _ny, std::string _st, int _samples,
         std::string _filt, Vector3& _eye, Vector3& _gaze, float _distance, 
         float _fnumber, float _focal, float _filmw, float _filmh, float _tmin,
         float _tmax) : filename(_filename), nx(_nx), ny(_ny), st(_st), samples(_samples),
         filt(_filt), eye(_eye), gaze(_gaze), distance(_distance), fnumber(_fnumber),
         focal(_focal), filmw(_filmw), filmh(_filmh), tmin(_tmin), tmax(_tmax) {}

Cli::Cli(int num_statement, char* clil[])
        {
            //default values in case there is no cli parameter
            filename = "image01.ppm";
	    nx = 640;
	    ny = 480;
            st = "jitter";
            samples = 1;
	    filt = "box";
	    eye.setX(.0f);
            eye.setY(.0f);
            eye.setZ(.0f);
	    gaze.setX(.0f);
	    gaze.setY(.0f);
	    gaze.setZ(-1.0f);
	    distance = 1.0f;
	    fnumber = 5.6f;
            focal = 0.05f;
            filmw = 0.036f;
            filmh = 0.024f;
	    tmin = 0.000001f;
	    tmax = 100000.0f;
            overwrite = false;
            debug = false;

            // here we override the previous initialized parameters
            for (int i = 1; i < num_statement; i++ ) 
            {
                if ( clil[i][0] == '-' )
                {
                    switch( clil[i][1] )
                    {
                        case 'F':
                            filename = clil[++i];
                            break;
                        case 'p':
                            nx = atoi(clil[++i]);
                            ny = atoi(clil[++i]);
                            break;
                        case 's':
                            st = clil[++i];
                            samples = atoi(clil[++i]);
                            break;
                        case 'f':
                            filt = clil[++i];
                            break;
                        case 'e':
                            eye.setX(atof(clil[++i]));
                            eye.setY(atof(clil[++i]));
                            eye.setZ(atof(clil[++i]));
                            break;
                        case 'g':
                            gaze.setX(atof(clil[++i]));
                            gaze.setY(atof(clil[++i]));
                            gaze.setZ(atof(clil[++i]));
                            break;
                        case 'd':
                            distance = atof(clil[++i]);
                            break;
                        case 'a':
                            fnumber = atof(clil[++i]);
                            break;
                        case 'c':
                            focal = atof(clil[++i]) / 1000.0f;
                            break;
                        case 'w':
                            filmw = atof(clil[++i]) / 1000.0f;
                            filmh = atof(clil[++i]) / 1000.0;
                            break;
                        case 'm':
                            tmin = atof(clil[++i]);
                            break;
                        case 'x':
                            tmax = atof(clil[++i]);
                            break;
                        case 'y':
                            overwrite = true;
                            break;
                        case 'D':
                            debug = true;
                            break;
                    }
                }
            }
        }  //issue 'undefined reference' was forgetting the {}

void Cli::showData() const
{
    std::cout << "filename:       " << filename << std::endl;
    std::cout << "hor. pixels:    " << nx       << std::endl;
    std::cout << "vert. pixels:   " << ny       << std::endl;
    std::cout << "sample filter:  " << st       << std::endl;
    std::cout << "samples:        " << samples  << std::endl;
    std::cout << "pixel filter:   " << filt     << std::endl;
    std::cout << "camera pos.:    " << eye      << std::endl;
    std::cout << "view direction: " << gaze     << std::endl;
    std::cout << "focus distance: " << distance << std::endl;
    std::cout << "f-number:       " << fnumber  << std::endl;
    std::cout << "film width:     " << filmw    << std::endl;
    std::cout << "film height:    " << filmh    << std::endl;
    std::cout << "min ray length: " << tmin     << std::endl;
    std::cout << "max ray length: " << tmax     << std::endl;
}

