// Cli.h

#ifndef _CLI_H_
#define _CLI_H_ 1

#include <cstdlib>
#include <string>
//#include <iostream>
#include <fstream>
#include "../math/Vector3.h"

class Cli
{
    public:
	Cli();

        Cli(std::string _filename, int _nx, int _ny, std::string _st, int _samples,
            std::string _filt, Vector3& _eye, Vector3& _gaze, float _distance, 
            float _fnumber, float focal, float filmw, float filmh, float _tmin,
            float _tmax);

	Cli(int num_statement, char* clil[]);

        void showData() const;

	std::string filename;	    //-F <string>               filename
	int nx;			    //-p <int>                  image pixel size hor
	int ny;			    //-p <int>                  image pixel size vert.
        std::string st;		    //-s <string>               pixel sampler
        int samples;		    //-s <int>                  number of samples
	std::string filt;           //-f <string>               pixel filter
	Vector3 eye;		    //-e <float float float>    position of camera
	Vector3 gaze;		    //-g <float float float>    viewing direction
	float distance;		    //-d  <float>               focal distance in m
	float fnumber;	    //-a <float>                lens fnumber in mm
        float focal;                //-c <float>                focal length in mm
        float filmw;                //-w <float>                film width in mm
        float filmh;                //-w <float>                film height in mm
	float tmin;		    //-m <float>                min ray distance
	float tmax;		    //-x <float>                max ray distance
	bool overwrite;		    //-y                        overwrite image yes or no
        bool debug;                 //-D                        debug flag

};

#endif //_CLI_H_
