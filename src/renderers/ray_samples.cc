// ray_samples.cc
// added a sampling and filter function from Samples.h
// added DynArray.h instead of a vector class to hold shapes

#include <iostream>
#include "../utilities/rgb.h"
#include "../utilities/DynArray.h"
#include "../imageio/Image.h"
#include "../math/Vector2.h"
#include "../math/Vector3.h"
#include "../math/Sample.h"
#include "../surfaces/Shape.h"
#include "../surfaces/Triangle.h"
#include "../surfaces/Sphere.h"

using namespace std;

int main(int argc, char* argv[]) {
    HitRecord rec;
    float hit_time = 0.0;	
    bool is_a_hit;
    float tmax;			// max valid t parameter
    Vector3 dir(0, 0, -1);	// direction of viewing rays

    // calculate samples
    // variables for samples
    int numsamples = 16;
    // samplelist is an array holding numsamples Vector2 objects
    Vector2 samplelist[numsamples];
    // jitter the samples
    jitter(samplelist, numsamples);
    // use the tentfilter to get a better average
    tentFilter(samplelist, numsamples);

    // geometry
    DynArray<Shape*> shapes;
    shapes.append( new Sphere( Vector3(250, 250, -1000), 150, 
	rgb(0.2, 0.2, 0.8) ) );
    shapes.append( new Triangle( Vector3(300.0f, 600.0f, -800), 
	Vector3(0.0f, 100.0f, -1000), Vector3(450.0f, 20.0f, -1000), rgb(0.8, 0.2, 0.2) ) );

    Image im(500, 500);

    // loop over pixels
    for ( int i = 0; i < 500; i++ )
	for ( int j = 0; j < 500; j++ ) {
	    rgb pix_color(.0f, .0f, .0f);
	    for ( int k = 0; k < numsamples; k++ ) {
		tmax = 100000.0f;
		is_a_hit = false;
		Ray r(Vector3(i + samplelist[k].x(), j +samplelist[k].y(), 0), dir);
	    
		// loop over list of shapes
		for( int k = 0; k < shapes.length(); k++)
		    if ( shapes[k]->hit(r, 0.000001f, tmax, hit_time, rec) ) {
			tmax = rec.t;
			is_a_hit = true;
		    }
	    
		if ( is_a_hit )
		    pix_color += rec.color;
		else
		    pix_color += rgb(0.2, 0.2, 0.2);
	    }
	    pix_color /= numsamples;
	    im.set(i,j, pix_color);	
	}
    im.writePPM(cout);
}
