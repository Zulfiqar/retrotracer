// raytrace.cc

#include <vector>		// stl vector thingy
#include <iostream>
#include "../utilities/rgb.h"
#include "../imageio/Image.h"
#include "../math/Vector3.h"
#include "../surfaces/Shape.h"
#include "../surfaces/Triangle.h"
#include "../surfaces/Sphere.h"

using namespace std;

int main() {
    HitRecord rec;
    float hit_time = 0.0;	
    bool is_a_hit;
    float tmax;			// max valid t parameter
    Vector3 dir(0, 0, -1);	// direction of viewing rays

    // geometry
    // why do we use the stl vector when we programmed our own dynamic array?
    vector<Shape*> shapes;
    shapes.push_back( new Sphere( Vector3(250, 250, -1000), 150, 
	rgb(0.2, 0.2, 0.8) ) );
    shapes.push_back( new Triangle( Vector3(300.0f, 600.0f, -800), 
	Vector3(0.0f, 100.0f, -1000), Vector3(450.0f, 20.0f, -1000), rgb(0.8, 0.2, 0.2) ) );

    Image im(500, 500);

    // loop over pixels
    for ( int i = 0; i < 500; i++ )
	for ( int j = 0; j < 500; j++ ) {
	    tmax = 100000.0f;
	    is_a_hit = false;
	    Ray r(Vector3(i, j, 0), dir);
	    
	    // loop over list of shapes
	    for( int k = 0; k < shapes.size(); k++)
		if ( shapes[k]->hit(r, 0.000001f, tmax, hit_time, rec) ) {
		    tmax = rec.t;
		    is_a_hit = true;
		}
	    
	    if ( is_a_hit )
		im.set(i, j, rec.color);
	    else
		im.set(i, j, rgb(0.2, 0.2, 0.2) );
	}
    im.writePPM(cout);
}
