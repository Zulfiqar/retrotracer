// ray_pinhole_uvw.cc
// added code to create a pinhole camera setup to create image 4.5
// pinhole camera with adjustable view and location

#include <iostream>
#include "../utilities/rgb.h"
#include "../utilities/DynArray.h"
#include "../utilities/Cli.h"
#include "../imageio/Image.h"
#include "../math/Vector2.h"
#include "../math/Vector3.h"
#include "../math/Sample.h"
#include "../surfaces/Shape.h"
#include "../surfaces/Sphere.h"
#include "../math/ONB.h"

//using namespace std;

int main(int argc, char* argv[]) 
{

    Cli carg(argc, argv);

    if ( carg.debug ){
        std::cout << "Render parameters:" << std::endl;
        carg.showData();
        std::cout << std::endl;
    }

    fstream image;                      //file object

    HitRecord rec;
    float hit_time = 0.0;	
    bool is_a_hit;
    float tmax;			        // max valid t parameter - max ray length

    // setup a camera
    Vector3 eye(.0f, .0f, 2.0f);        // pinhole position
    Vector3 gaze(.0f, .0f, -2.0f);
    Vector3 vup(.0f, 1.0f, .0f);
    int nx          =  101;             // image width
    int ny          =  101;             // image height
    Image im(nx, ny);

    // setup world space
    Vector3 w = - gaze / gaze.length();
    Vector3 u = cross( vup, w) / ((cross(vup, w)).length());
    Vector3 v = cross(w, u);
    ONB world(u, v, w);

    // set up screen
    float distance  =  2.0;           // s parameter
    float left      = -2.0;
    float right     =  2.0;
    float bottom    = -2.0;
    float top       =  2.0;
    Vector3 v_a = (right - left) * world.u();
    Vector3 v_b = (top - bottom) * world.v();
    Vector3 v_c = left * world.u() + bottom * world.v() - distance * world.w();

    // geometry, a single sphere with basecolor 0.1 for rgb
    DynArray<Shape*> shapes;
    shapes.append( new Sphere( Vector3(.0f, .0f, .0f), sqrt(2.0f), 
	rgb(0.1, 0.1, 0.1) ) );

    float a, b;
    // loop over pixels
    for ( int i = 0; i < nx; i++ )
	for ( int j = 0; j < ny; j++ ) 
        {
	    rgb pix_color(.0f, .0f, .0f);
            a = (i + 0.5f) / nx;
            b = (j + 0.5f) / ny;
 
            tmax = 100000.0f;
	    is_a_hit = false;

            Vector3 s = v_c + a * v_a + b * v_b;
            Ray vray(eye, s);

            // loop over list of shapes
	    for( int k = 0; k < shapes.length(); k++)
            {
	        if ( shapes[k]->hit(vray, 0.000001f, tmax, hit_time, rec) ) 
                {
	            tmax = rec.t;
		    is_a_hit = true;
		}
            }
	    
            if ( is_a_hit ) 
            {
                Vector3 ns = rec.normal;
                ns.makeUnitVector();
                float addc = 0.9 * dot(ns, vup);
                if (addc > 0.0f )
                {
                    rgb add_color(addc, addc, addc);
                    pix_color += rec.color + add_color;
                }
                else
		    pix_color += rec.color;
	    } 
            else 
            {
	        pix_color += rgb(0.0, 0.0, 0.0);
            }

	    im.set(i,j, pix_color);	
	}

    im.writePPM( std::cout );
}
