// ray_camera.cc
// added code to create a thin lens camera setup to create
// something like images 4.8 and 4.9 (without texturing)

#include <iostream>
#include <vector>
//#include <iterator>
#include <fstream>
#include "../utilities/rgb.h"
#include "../utilities/DynArray.h"
#include "../imageio/Image.h"
#include "../math/Vector2.h"
#include "../math/Vector3.h"
#include "../math/Sample.h"
#include "../surfaces/Shape.h"
#include "../surfaces/Sphere.h"
#include "../math/ONB.h"
#include "../cameras/Camera.h"
#include "../utilities/Cli.h"


int main(int argc, char* argv[]) 
{
    //std::vector< std::string > arguments( argv, argv+argc );
    //std::copy(arguments.begin(), arguments.end(), std::ostream_iterator< std::string >(std::cout, "\n"));

    Cli carg(argc, argv);

    if ( carg.debug ){
        std::cout << "Render parameters:" << std::endl;
        carg.showData();
        std::cout << std::endl;
    }
    
    fstream image;              // file object
    HitRecord rec;
    float hit_time = 0.0;	
    bool is_a_hit;
    float tmax;			// max valid t parameter - max ray length
    float tmin = carg.tmin;     // ignore rays smaller than tmin

    // setup world space
    Vector3 u(1.0f, .0f, .0f);
    Vector3 v(.0f, 1.0f, .0f);
    Vector3 w(.0f, .0f, 1.0f);

    Vector3 eye = carg.eye;      // pinhole position
    Vector3 gaze = carg.gaze;
    Vector3 vup(.0f, 1.0f, .0f);
    float fnumber  = carg.fnumber;
    float focal    = carg.focal;
    float aperature = focal / (2.0f * fnumber);
    // set up screen
    float distance  =  carg.distance;           // s parameter
    //float left      = -2.0;
    //float right     =  2.0;
    //float bottom    = -2.0;
    //float top       =  2.0;
    float right  = (carg.filmw * 0.5f) * (distance - focal) / focal;
    float left   = -right;
    float top    = (carg.filmh * 0.5f) * (distance - focal) / focal;
    float bottom = -top;
    // setup a camera
    Camera Cam1(eye, gaze, vup, aperature, left, right, bottom, top, distance);

    // sampling
    int numsamples = carg.samples;
    // camera sampling points
    Vector2 samplescam[numsamples];
    // image sampling points
    Vector2 samplepix[numsamples];
    jitter(samplepix, numsamples);
    boxFilter(samplepix, numsamples);

    // image parameters
    int nx          =  carg.nx;           // image width
    int ny          =  carg.ny;           // image height
    Image im(nx, ny);

    // get camera space
    ONB csp = Cam1.uvw;
    std::cout << "Camera space coordinates:" << std::endl;
    std::cout << csp << std::endl;
 
    // geometry, a single sphere with basecolor 0.1 for rgb
    DynArray<Shape*> shapes;
    // place spheres in camera space (we don't have transforms yet)
    shapes.append( new Sphere( Vector3(.0f, .0f, .0f), 0.01f, rgb(1.0, 0.3, 0.6) ) );
    shapes.append( new Sphere( Vector3(.0f, 4.0f, -3.0f), 0.5f, rgb(1.0, 0.1, 0.1) ) );
    shapes.append( new Sphere( Vector3(2.0f, .0f, -3.0f), 0.5f, rgb(0.1, 1.0, 0.1) ) );
    shapes.append( new Sphere( Vector3(-2.0f, .0f, -3.0f),0.5f, rgb(0.1, 0.1, 1.0) ) );
    shapes.append( new Sphere( Vector3(3.0f, 3.0f, -4.0f),0.5f, rgb(1.0, 1.0, 1.0) ) );

    float a, b;
    // loop over pixels
    for ( int i = 0; i < nx; i++ )
	for ( int j = 0; j < ny; j++ ) 
        {
	    rgb pix_color(.0f, .0f, .0f);
 
            tmax = carg.tmax;
	    is_a_hit = false;

            //camera samples calculated for each pixel
            jitter(samplescam, numsamples);

            //calculate screen coordinates
            a = (i + 0.5) / nx;
            b = (j + 0.5) / ny;
            
            //loop over camera samples - for each sample
            for ( int k = 0; k < numsamples; k++ ) {
                

                Ray vray = Cam1.getRay(a + samplepix[k].x(), b + samplepix[k].y(),
                                       samplescam[k].x(), samplescam[k].y());

                // loop over list of shapes
	        for( int k = 0; k < shapes.length(); k++)
                {
	            if ( shapes[k]->hit(vray, tmin, tmax, hit_time, rec) ) 
                    {
	                tmax = rec.t;
		        is_a_hit = true;
		    }
                }
	    
                if ( is_a_hit ) 
                {
                    Vector3 ns = rec.normal;
                    ns.makeUnitVector();
                    float addc = 0.9 * dot(ns, vup);
                    if (addc > 0.0f )
                    {
                        rgb add_color(addc, addc, addc);
                        pix_color += rec.color + add_color;
                    }
                    else
		        pix_color += rec.color;
	        } 
                else 
                {
	            pix_color += rgb(0.0, 0.0, 0.3); //background color
                }
            }
            pix_color /= numsamples;
	    im.set(i,j, pix_color);	// write to buffer
	}

    im.createFile(image, carg.filename, carg.overwrite);
    im.writePPM(image);
    return 0;
}
