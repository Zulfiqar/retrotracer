// Camera.cc

#include "Camera.h"

Camera::Camera( Vector3 c, Vector3 gaze, Vector3 vup, float aperature, float left,
		    float right, float bottom, float top, float distance ) :
		    center(c), u0(left), u1(right), v0(bottom), v1(top), d(distance) {
    lens_radius = aperature / 2.0F;
    uvw.initFromWU( -gaze, vup );
    corner = center + u0 * uvw.u() + v0 * uvw.v() - d * uvw.w();
    across = ( u0 - u1 ) * uvw.u();
    up = (v0 - v1) * uvw.v();
}

// a and b are the pixel positions
// xi1 and xi2 are the lens samples in the range (0, 1)^2
Ray Camera::getRay( float a, float b, float xi1, float xi2 ) {
 	Vector3 origin = center + 2.0F * (xi1 - 0.5F) * lens_radius * uvw.u() +
			2.0F * (xi2 - 0.5F) * lens_radius * uvw.v();
	Vector3 target = corner + across * a + up * b;
	return Ray( origin, unitVector( target - origin ) );
}
