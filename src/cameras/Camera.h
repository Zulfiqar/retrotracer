// Camera.h

#ifndef _CAMERA_H_
#define _CAMERA_H_ 1

#include "../math/Ray.h"
#include "../math/ONB.h"

class Camera {
    public:
	Camera() {}
	Camera( const Camera& orig ) {
	    center = orig.center;
	    corner = orig.corner;
	    across = orig.across;
	    up	   = orig.up;
	    uvw    = orig.uvw;
	    u0     = orig.u0;
	    u1     = orig.u1;
	    v0     = orig.v0;
	    v1     = orig.v1;
	    d      = orig.d;
	    lens_radius = orig.lens_radius;
	}

	Camera( Vector3 c, Vector3 gaze, Vector3 vup, float aperature, float left,
	       float right, float bottom, float top, float distance );
	Ray getRay( float a, float b, float xi1, float xi2 );

	Vector3 center, corner, across, up;
	ONB uvw;
	float lens_radius;
	float u0, u1, v0, v1;
	float d;
};

#endif // _CAMERA_H_
